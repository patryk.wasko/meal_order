class Order < ApplicationRecord
  validates :restaurant_name, presence: true
  validates :status, presence: true
 
  scope :active, -> { where(status: [:started, :finalized, :ordered]) }
  scope :archived, -> { where(status: :delivered)}

  enum status: [:started, :finalized, :ordered, :delivered]
 
  has_many :meals, dependent: :destroy
end
