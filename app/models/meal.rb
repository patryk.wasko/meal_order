class Meal < ApplicationRecord
  validates :name, presence: true
  validates :price, presence: true, numericality: true
  validates :user, presence: true
  validates :order, presence: true
  validates :order, uniqueness: {scope: :user, message: "You can add only one meal to order." }
  validate :validate_order_status, on: :create
  
  belongs_to :order
  belongs_to :user

  private

  def validate_order_status
    if self.order 
      errors.add(:order, "You can add meal only to not finalized order") unless self.order.started? 
    end
  end
end
