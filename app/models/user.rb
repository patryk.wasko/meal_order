class User < ApplicationRecord
  validates :auth_token, uniqueness: true
  validates :provider_uid, uniqueness: { scope: :provider }

  has_many :meals, dependent: :destroy
end
