class Api::V1::BaseController < ApplicationController
  protect_from_forgery with: :null_session

  def authenticate_token
    authenticate_with_http_token do |token|
      User.find_by(auth_token: token)
    end
  end

  def current_user
    @current_user ||= authenticate_token
  end

  def authenticate_user
    raise NoAuthorizationError unless current_user
  end

  class NoAuthorizationError < StandardError
  end 

  class SessionExpiredError < StandardError
  end 

  rescue_from SessionExpiredError do |exception|
    render json: { message: 'Session Expired' }, status: 401
  end

  rescue_from NoAuthorizationError do |exception|
    render json: { message: 'Authentication Error' }, status: 401
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    render json: { message: 'Record not found' }, status: 404
  end
end
