class Api::V1::MealsController < Api::V1::BaseController
  before_action :authenticate_user
  before_action :check_owner, only: [:update, :destroy]

  def create
    meal = current_user.meals.create(meal_params)
    if meal.save
      render json: { meal: meal.serializable_hash(include: { user: { only: [:id,
                                                                            :name,
                                                                            :email,
                                                                            :image] }}
                                                 ), message: 'Meal created' }
    else
      render json: { message: meal.errors.full_messages.join(', ') }, status: 400
    end
  end


  def update
    #without this it doesn't return error message
    meal_to_update = meal 
    if meal_to_update.update(meal_params)
      render json: { meal: meal_to_update.serializable_hash(include: { user: { only: [:id,
                                                                            :name,
                                                                            :email,
                                                                            :image] }}
                                                 ), message: 'Meal updated' }
    else
      render json: { message: meal_to_update.errors.full_messages.join(', ') }, status: 400
    end 
  end

  def destroy
    meal.destroy
    render json: { message: 'Meal deleted' }, status: 200
  end

  private
  def meal
    Meal.find(params[:id])
  end
  
  def meal_params
    params.require(:meal).permit(:name, :price, :order_id)
  end
  
  def check_owner
    raise NotOwnerError unless current_user.id == meal.user.id
 #   raise NotOwnerError
  end

  class NotOwnerError < StandardError
  end 

  rescue_from NotOwnerError do |exception|
    render json: { message: 'Only owner can do that' }, status: 401
  end
end
