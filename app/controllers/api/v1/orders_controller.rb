class Api::V1::OrdersController < Api::V1::BaseController
  before_action :authenticate_user

  def index 
    page = params[:page] ? params[:page].to_i - 1 : 0
    orders = Order.active.order(id: :desc).offset(page * 10).limit(10)
    render json: { orders: orders }
  end

  def archived
    page = params[:page] ? params[:page].to_i - 1 : 0
    orders = Order.archived.order(id: :desc).offset(page * 10).limit(10)
    render json: { orders: orders }
  end
   
  def show
    render json: { order: order.serializable_hash(include: { meals: { only: [:id,
                                                                             :name,
                                                                             :price,
                                                                             'user.name'],
                                                                     include: {user: {only: [ :id, :name, :email, :image]}}}})}
  end 
  

## Order.last.serializable_hash(include: [meals: {include: :user }]   )

  def create
    order = Order.create(order_params)
    if order.save
      render json: {order: order, message: 'Order created'}
    else
      render json: {message: order.errors.full_messages.join(', ')}, status: 400
    end
  end

  def update
    #without this it doesn't return error message
    order_to_update = order 

    if order_to_update.update(order_params)
      render json: {order: order_to_update, message: 'Order updated'}
    else
      render json: {message: order_to_update.errors.full_messages.join(', ')}, status: 400
    end
  end
 
  def destroy
    order.destroy
    render json: {message: 'Order deleted'}
  end
  
  def change_status
    new_status = params[:status]
    valid_statuses = ['started', 'finalized', 'delivered', 'ordered']
    raise ForbiddenParamError unless valid_statuses.include?(new_status)
    order.method("#{new_status}!".to_sym).call
    render json: {message: 'Status updated', order: order}
  end

  private
  def order 
    Order.find(params[:id])
  end
  
  def order_params
    params.require(:order).permit(:restaurant_name)
  end

  class ForbiddenParamError < StandardError
  end 

  rescue_from ForbiddenParamError do |exception|
    render json: { message: 'This param is unforbidden' }, status: 400
  end

end
