class Api::V1::SessionsController < Api::V1::BaseController
  protect_from_forgery with: :null_session
  
  def create
    user = User.find_by_auth_token(params[:auth_token])
    raise NoAuthorizationError unless user   

    render json:  { message: 'Signed In', 
                    user: {id: user.id,
                           name: user.name,
                           email: user.email,
                           image: user.image,
                           provider: user.provider,
                           auth_token: user.auth_token
                         } 
                  }
  end

  def check
    raise SessionExpiredError unless current_user   
    render json: { message: 'Signed In' }
  end
end

