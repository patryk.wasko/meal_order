class SessionCreatorController < ApplicationController
  def create_session
    begin 
      raise InvalidOmniauthHash unless request.env['omniauth.auth']
      create_user_service = CreateUserService.new(request.env['omniauth.auth'])
      user = create_user_service.create_from_omniauth
      redirect_to session_creator_path auth_token: user.auth_token
    rescue InvalidOmniauthHash
      redirect_to session_creator_path
    end
  end

  class InvalidOmniauthHash < StandardError
  end 
end
