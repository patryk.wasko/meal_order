import * as types from './mutation-types'

export const mutations = {
  [types.SIGN_IN] (state, userPayload) {
    state.user = userPayload
    state.logged = true
    window.axios.defaults.headers.common = {
      'Authorization': `Bearer ${state.user.auth_token}`
    };

  },

  [types.SIGN_OUT] (state) {
    state.user = {}
    state.orders = []
    state.meals = []
    state.logged = false
    window.axios.defaults.headers.common = {
      'Authorization': `Bearer `
    };
  },

  [types.LOAD_ORDERS] (state, ordersPayload) {
    state.orders = ordersPayload
  },

  [types.DESTROY_ORDER] (state, orderPayload) {
    const index = state.orders.findIndex(obj => obj.id === orderPayload.id);
    state.orders = [
      ...state.orders.slice(0, index),
      ...state.orders.slice(index + 1)
    ]
  },

  [types.ADD_ORDER] (state, orderPayload) {
    state.orders.unshift(orderPayload)
  },

  [types.UPDATE_ORDER] (state, orderPayload) {
    const index = state.orders.findIndex(obj => obj.id === orderPayload.id);
    state.orders = [
      ...state.orders.slice(0, index),
      orderPayload,
      ...state.orders.slice(index + 1)
    ]
  },

  [types.LOAD_MEALS] (state, mealsPayload) {
    state.meals = mealsPayload
  },

  [types.ADD_MEAL] (state, mealPayload) {
    state.meals.push(mealPayload.meal)
  },

  [types.DESTROY_MEAL] (state, mealPayload) {
    const index = state.meals.findIndex(obj => obj.id === mealPayload.id);
    state.meals = [
      ...state.meals.slice(0, index),
      ...state.meals.slice(index + 1)
    ]
  },

  [types.UPDATE_MEAL] (state, mealPayload) {
    const index = state.meals.findIndex(obj => obj.id === mealPayload.id);
    state.meals = [
      ...state.meals.slice(0, index),
      mealPayload,
      ...state.meals.slice(index + 1)
    ]
  }
} 
