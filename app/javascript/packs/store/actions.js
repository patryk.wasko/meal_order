import * as types from './mutation-types.js'

export const signIn = ({ commit }, userPayload) => {
  commit(types.SIGN_IN, userPayload)
}

export const signOut = ({ commit }) => {
  commit(types.SIGN_OUT)
}

export const loadOrders = ({ commit }, ordersPayload) => {
  commit(types.LOAD_ORDERS, ordersPayload)
}

export const destroyOrder = ({ commit }, orderPayload) => {
  commit(types.DESTROY_ORDER, orderPayload)
}

export const addOrder = ({ commit }, orderPayload) => {
  commit(types.ADD_ORDER, orderPayload)
}

export const updateOrder = ({ commit }, orderPayload) => {
  commit(types.UPDATE_ORDER, orderPayload)
}

export const loadMeals = ({ commit }, mealsPayload) => {
  commit(types.LOAD_MEALS, mealsPayload)
}

export const addMeal = ({ commit }, mealPayload) => {
  commit(types.ADD_MEAL, mealPayload)
}

export const updateMeal = ({ commit }, mealPayload) => {
  commit(types.UPDATE_MEAL, mealPayload)
}

export const destroyMeal = ({ commit }, mealPayload) => {
  commit(types.DESTROY_MEAL, mealPayload)
}
