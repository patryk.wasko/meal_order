import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store/'

import App from './app.vue'
import Dashboard from './components/dashboard.vue'

import SignIn from './components/sessions/signIn.vue'
import FacebookButton from './components/sessions/facebookButton.vue'
import GithubButton from './components/sessions/githubButton.vue'
import SignOutButton from './components/sessions/signOutButton.vue'
import UserPanel from './components/sessions/userPanel.vue'
import SessionCreator from './components/sessions/sessionCreator.vue'

import Orders from './components/orders/orders.vue'
import Order from './components/orders/order.vue'
import AddOrderButton from './components/orders/buttons/addOrderButton.vue'
import UpdateOrderButton from './components/orders/buttons/updateOrderButton.vue'
import DeleteOrderButton from './components/orders/buttons/deleteOrderButton.vue'
import ChangeOrderStatusButton from './components/orders/buttons/changeOrderStatusButton.vue'
import OrderPage from './components/orders/orderPage.vue'

import Meals from './components/meals/meals.vue'
import Meal from './components/meals/meal.vue'
import AddMealButton from './components/meals/buttons/addMealButton.vue'
import DeleteMealButton from './components/meals/buttons/deleteMealButton.vue'
import UpdateMealButton from './components/meals/buttons/updateMealButton.vue'

import SharedTopbar from './components/shared/sharedTopbar.vue'
import SharedLeftbar from './components/shared/sharedLeftbar.vue'

import Modal from './components/modals/modal.vue'
import ErrorModal from './components/modals/errorModal.vue'

Vue.component('shared-topbar', SharedTopbar)
Vue.component('shared-leftbar', SharedLeftbar)
Vue.component('sign-in', SignIn)
Vue.component('facebook-button', FacebookButton)
Vue.component('sign-out-button', SignOutButton)
Vue.component('github-button', GithubButton)
Vue.component('user-panel', UserPanel)

Vue.component('modal', Modal)
Vue.component('error-modal', ErrorModal)

Vue.component('order', Order)
Vue.component('add-order-button', AddOrderButton)
Vue.component('delete-order-button', DeleteOrderButton)
Vue.component('update-order-button', UpdateOrderButton)
Vue.component('change-order-status-button', ChangeOrderStatusButton)
Vue.component('add-meal-button', AddMealButton)

Vue.component('meals', Meals)
Vue.component('meal', Meal)
Vue.component('delete-meal-button', DeleteMealButton)
Vue.component('update-meal-button', UpdateMealButton)

Vue.use(VueRouter)

import axios from 'axios';

export const HTTP = axios.create({
})
window.axios = HTTP;

window.axios.defaults.headers.common = {
  'Authorization': `Bearer ${store.state.user.auth_token}`
};
window.axios.defaults.baseURL = '/';
Vue.prototype.$http = window.axios

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/sign_in', component: SignIn },
    { path: '/', component: Orders },
    { path: '/orders/:id', component: OrderPage },
    { path: '/session_creator', component: SessionCreator },
    { path: '/orders', component: Orders }
  ]
})

document.addEventListener('DOMContentLoaded', () => {
  document.body.appendChild(document.createElement('vue'))
  const app = new Vue({
    el: 'vue',
    router,
    template: '<App/>',
    store,
    render: h => h(App)
  })
})
