class CreateUserService
  def initialize(omniauth_hash)
    @omniauth_hash = omniauth_hash
  end
  def create_from_omniauth
    user = User.where(provider_uid: @omniauth_hash['uid'], provider: @omniauth_hash['provider']).first_or_create do |user|
      user.provider = @omniauth_hash['provider']
      user.provider_uid = @omniauth_hash['uid']
      user.email = @omniauth_hash['info']['email']
      user.name = @omniauth_hash['info']['name']
      user.image = @omniauth_hash['info']['image']
      user.auth_token = SecureRandom.hex   
    end 
  end
end
