# README
## Meal Order App:

SPA app to coordinate meal orders

## Before start:
  - set local variables for omniauth
  -- FACEBOOK_KEY
  -- FACEBOOK_SECRET
  -- GITHUB_KEY
  -- GITHUB_SECRET
  - run:
    ```
      $ yarn install
      $ bundle install
      $ bundle exec rake db:create
      $ bundle exec rake db:migrate
    ```
## To run tests
 ```
 $ rspec spec/
 ```
## To run dev server:
```
$ foreman start -f Procfile.dev
```
## Main libraries:
* Ruby on Rails 5.1.4
* Vue 2.4.4
* Postgresq
* Webpack
* Webpacker
* RSpec
* Capybara

## App avaible on:
[MealOrderApp](https://meal-order.herokuapp.com/)
