module OmniauthSupport
  def mock_omniauth_hash(provider = 'facebook')
    OmniAuth.config.mock_auth[provider.to_sym] = {
          'provider' => provider,
          'uid' => 123,
          'info' => {
            'name' =>  Faker::Name.name ,
            'email' => "#{provider}@email.com",
            'image' => ''
          },
          'credentials' => {
            'token' => 'mock_token',
            'secret' => 'mock_secret'
          }
    }
  end
end
