module LoginSupport
  def sign_in_as_user(user)
    omniauth_hash = mock_omniauth_hash('facebook')
    user.update!(provider: omniauth_hash['provider'], provider_uid: omniauth_hash['uid'])
    visit '/'
    within('.login_container') do
      page.find('.btn.btn-facebook').click
    end
  end
end
