module ErrorsSupport
  def expect_authentication_error(response)
    json = JSON.parse(response.body)
    expect(response).to have_http_status(401)
    expect(json['message']).to eq('Authentication Error')
  end
   
  def expect_record_not_found_error(response)
    json = JSON.parse(response.body)
    expect(response).to have_http_status(404)
    expect(json['message']).to eq('Record not found')
  end
end
