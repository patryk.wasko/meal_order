require 'rails_helper'

RSpec.describe SessionCreatorController, type: :controller do
  let(:user) { FactoryGirl.create :user }

  describe 'GET :create_session' do

    it 'return user for existing user ' do
      user
      user.update(provider: mock_omniauth_hash['provider'],
                  provider_uid: mock_omniauth_hash['uid'])
      request.env['omniauth.auth'] = mock_omniauth_hash
      expect{ get :create_session, params: { provider: mock_omniauth_hash['provider']} }.to_not change{User.count}
      expect(response).to redirect_to session_creator_path auth_token: User.find_by(provider: mock_omniauth_hash['provider'], provider_uid: mock_omniauth_hash['uid']).auth_token
    end

    it 'return user hash for valid user params' do
      request.env['omniauth.auth'] = mock_omniauth_hash
      expect{ get :create_session, params: { provider: mock_omniauth_hash['provider']} }.to change{User.count}.by 1
      expect(response).to redirect_to session_creator_path auth_token: User.find_by(provider: mock_omniauth_hash['provider'], provider_uid: mock_omniauth_hash['uid']).auth_token
    end
   
    it 'return error when credentials are invalid' do
      OmniAuth.config.mock_auth[:facebook] = :invalid_credentials
      get :create_session, params: { provider: mock_omniauth_hash['provider']}
      expect(response).to have_http_status(302)
      expect(response).to redirect_to session_creator_path
    end
  end
end
