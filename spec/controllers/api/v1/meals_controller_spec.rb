require 'rails_helper'

RSpec.describe Api::V1::MealsController, type: :request do
  let(:user) { FactoryGirl.create :user }
  let(:order) { FactoryGirl.create :order }
  let(:meal) { FactoryGirl.create :meal }
 
  let(:new_meal_name) { 'new_meal_name' } 
  let(:new_price) { 12.32 }

  describe 'POST :create' do

    it 'create meal for valid params' do
      expect{ post '/api/v1/meals', 
                    params: { meal: { order_id: order.id, 
                                      name: new_meal_name,
                                      price: new_price }}, 
                    headers: { Authorization: "Bearer #{user.auth_token}"}
             }.to change{Meal.count}.by(1)
      last_meal = Meal.last
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Meal created')
      expect(json['meal']).to match(JSON.parse({ id: last_meal.id,
                                      name: last_meal.name,
                                      price: last_meal.price,
                                      order_id: order.id,
                                      created_at: last_meal.created_at,
                                      updated_at: last_meal.updated_at,
                                      user_id: last_meal.user_id,
                                      user: { id: user.id,
                                              name: user.name,
                                              image: user.image,
                                              email: user.email }}.to_json))
   end

   it 'return error for invalid params' do
      expect{ post '/api/v1/meals', 
                    params: { meal: { order: '', 
                                      name: '',
                                      price: '' }}, 
                    headers: { Authorization: "Bearer #{user.auth_token}"}
             }.to_not change{Meal.count}
      json = JSON.parse(response.body)
      expect(response).to have_http_status(400)
      expect(json['message']).to eq("Name can't be blank, Price can't be blank, Price is not a number, Order can't be blank, Order must exist")
    end
  
    it 'return authentication error for user with invalid authorization' do
      post '/api/v1/meals', 
            params: { meal: { order: order, 
                              name: new_meal_name,
                              price: new_price }}, 
            headers: { Authorization: "Bearer wrong_token"}
      expect_authentication_error(response)
    end
  end

  describe 'PATCH :update' do

    it 'update meal for valid params' do 
      meal.update_column(:user_id, user.id)
      expect{ patch "/api/v1/meals/#{meal.id}", 
                    params: { meal: { price: new_price }}, 
                    headers: { Authorization: "Bearer #{user.auth_token}"}
             }.to change{Meal.find(meal.id).price}
      last_meal = Meal.last
      meal.reload
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Meal updated')
      expect(json['meal']).to match(JSON.parse({ id: last_meal.id,
                                      name: meal.name,
                                      price: new_price,
                                      order_id: meal.order.id,
                                      created_at: meal.created_at,
                                      updated_at: meal.updated_at,
                                      user_id: meal.user_id,
                                      user: { id: user.id,
                                              name: user.name,
                                              image: user.image,
                                              email: user.email }}.to_json))
   end

   it 'return error when update another user meal' do 
      expect{ patch "/api/v1/meals/#{meal.id}", 
                    params: { meal: { price: new_price }}, 
                    headers: { Authorization: "Bearer #{user.auth_token}"}
             }.to_not change{Meal.find(meal.id).price}
      last_meal = Meal.last
      meal.reload
      json = JSON.parse(response.body)
      expect(response).to have_http_status(401)
      expect(json['message']).to eq('Only owner can do that')
   end

   it 'return authentication error for user with invalid authorization' do
      patch "/api/v1/meals/#{meal.id}", 
            params: { meal: { order_id: order.id, 
                              name: new_meal_name,
                              price: @price }}, 
            headers: { Authorization: "Bearer wrong_token"}
      expect_authentication_error(response)
    end

    it 'return error for invalid params' do
      meal.update_column(:user_id, user.id)
      expect{ patch "/api/v1/meals/#{meal.id}", 
                    params: { meal: { order_id: '', 
                                      name: '',
                                      price: '' }}, 
                    headers: { Authorization: "Bearer #{user.auth_token}"}
             }.to_not change{meal}
      json = JSON.parse(response.body)
      expect(response).to have_http_status(400)
      expect(json['message']).to eq("Name can't be blank, Price can't be blank, Price is not a number, Order can't be blank, Order must exist")
    end
  end

  describe 'DELETE :destroy' do
    it 'delete meal for valid authentications' do
      meal.update_column(:user_id, user.id)
      expect{ delete "/api/v1/meals/#{meal.id}", 
                      headers: { Authorization: "Bearer #{user.auth_token}"}
            }.to change{Meal.count}.by(-1)
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Meal deleted')
    end

    it 'return error when delete another user meal' do
      meal
      expect{ delete "/api/v1/meals/#{meal.id}", 
                      headers: { Authorization: "Bearer #{user.auth_token}"}
            }.to_not change{Meal.count}
      json = JSON.parse(response.body)
      expect(response).to have_http_status(401)
      expect(json['message']).to eq('Only owner can do that')
    end

    it 'return error for invalid params' do 
      meal
      delete '/api/v1/meals/invalid_id', headers: { Authorization: "Bearer #{user.auth_token}"}
      expect_record_not_found_error(response)
    end
  end
end
