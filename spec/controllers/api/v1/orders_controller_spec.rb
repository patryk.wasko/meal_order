require 'rails_helper'

RSpec.describe Api::V1::OrdersController, type: :request do
  let(:user) { FactoryGirl.create :user }
  let(:order) { FactoryGirl.create :order }
  let(:meal) { FactoryGirl.create :meal }
  let(:order_finalized) { FactoryGirl.create :order, status: :finalized }
  let(:order_ordered) { FactoryGirl.create :order, status: :ordered }
  let(:order_delivered) { FactoryGirl.create :order, status: :delivered }
  let(:new_restaurant_name) { 'new restaurant name' }

  describe 'GET :index' do 
    before :each do 
      order
      order_ordered
      order_finalized
      order_delivered
    end

    it 'return only active orders for active param if authenticated' do
      get '/api/v1/orders', headers: { Authorization: "Bearer #{user.auth_token}" }
      json = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(json['orders']).to match(JSON.parse([order_finalized,
                                                  order_ordered,
                                                  order].to_json))
    end

    it 'return only archived orders for active param' do
      get '/api/v1/orders_archived', headers: { Authorization: "Bearer #{user.auth_token}" }
      json = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(json['orders']).to match(JSON.parse([order_delivered].to_json))
    end

    it 'return authentication error for user with invalid authorization' do
      get '/api/v1/orders', headers: { Authorization: "Bearer wrong_token" }
      expect_authentication_error(response)
    end  
  end 

  describe 'GET :show' do
    before :each do 
      order
    end

    it 'return order record for valid params' do
      meal.update(order_id: order.id, user_id: user.id)
      get "/api/v1/orders/#{order.id}", headers: { Authorization: "Bearer #{user.auth_token}" }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['order']).to match(JSON.parse({ id: order.id,
                                                  restaurant_name: order.restaurant_name,
                                                  status: order.status,
                                                  created_at: order.created_at,
                                                  updated_at: order.updated_at,
                                                  meals: [{ id: meal.id,
                                                            name: meal.name,
                                                            price: meal.price,
                                                            user: { id: user.id,
                                                                    name: user.name,
                                                                    image: user.image,
                                                                    email: user.email}}]}.to_json))

    end 
 
    it 'return authentication error for user with invalid authorization' do
      get "/api/v1/orders/#{order.id}", headers: { Authorization: "Bearer wrong_token" }
      expect_authentication_error(response)
    end  

    it 'return error for invalid order id' do
      get "/api/v1/orders/invalid_id", headers: { Authorization: "Bearer #{user.auth_token}" }
      expect_record_not_found_error(response)
    end  
  end

  describe 'POST :create' do
    it 'create order for valid params' do
      restaurant_name = 'new restaurant name'
      expect{post '/api/v1/orders', 
                  params: { order: { restaurant_name: restaurant_name }}, 
                  headers: { Authorization: "Bearer #{user.auth_token}"}
            }.to change{Order.count}.by(1)
      last_order = Order.last
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Order created')
      expect(json['order']).to match(JSON.parse(last_order.to_json))
    end

    it 'return error for invalid params' do
      restaurant_name = ''
      expect{ post '/api/v1/orders', 
                   params: { order: { restaurant_name: restaurant_name } }, 
                   headers: { Authorization: "Bearer #{user.auth_token}"}
            }.to_not change{Order.count}
      json = JSON.parse(response.body)
      expect(response).to have_http_status(400)
      expect(json['message']).to eq("Restaurant name can't be blank")
 
    end
  
    it 'return authentication error for user with invalid authorization' do
      restaurant_name = 'new restaurant name'
      post "/api/v1/orders", params: { order: { restaurant_name: restaurant_name }},
                             headers: { Authorization: "Bearer wrong_token" }
      expect_authentication_error(response)
    end  
  end

  describe 'DELETE :destroy' do
    before :each do 
      order
    end

    it 'delete order valid authentication' do
      expect{ delete "/api/v1/orders/#{order.id}", 
                     headers: { Authorization: "Bearer #{user.auth_token}" }
            }.to change{ Order.count }.by(-1)
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq("Order deleted")
    end
  
    it 'dont delete order without valid authentication' do
      expect{ delete "/api/v1/orders/#{order.id}",
                      headers: { Authorization: 'Bearer wrong_token' }
            }.to_not change{ Order.count }
      expect_authentication_error(response)
    end
    
    it 'return error for invalid order id' do
      expect{ delete "/api/v1/orders/wrong_id", 
                     headers: { Authorization: "Bearer #{user.auth_token}" }
            }.to_not change{ Order.count }
      expect_record_not_found_error(response)
    end
  end
  
  describe 'PATCH :update' do 
    before :each do 
      order
    end

    it 'update order with valid params' do
      expect{patch "/api/v1/orders/#{order.id}", 
                  params: { order: { restaurant_name: new_restaurant_name }}, 
                  headers: { Authorization: "Bearer #{user.auth_token}"}
            }.to change{Order.find(order.id).restaurant_name}.from(order.restaurant_name).to(new_restaurant_name)
      updated_order = Order.find(order.id)
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Order updated')
      expect(json['order']).to match(JSON.parse(updated_order.to_json))
    end
    
    it 'return error for invalid order id' do
      patch "/api/v1/orders/invalid_id", 
            params: { order: { restaurant_name: new_restaurant_name }}, 
            headers: { Authorization: "Bearer #{user.auth_token}"}
      expect_record_not_found_error(response)
    end

    it 'return error for invalid authentication' do
      patch "/api/v1/orders/#{order.id}", 
            params: { order: { restaurant_name: new_restaurant_name }}, 
            headers: { Authorization: "Bearer invalid_token"}
      expect_authentication_error(response)
    end
    
    it 'return error for invalid params' do
      expect{patch "/api/v1/orders/#{order.id}", 
                  params: { order: { restaurant_name: '' }}, 
                  headers: { Authorization: "Bearer #{user.auth_token}"}
            }.to_not change{Order.find(order.id).restaurant_name}
      json = JSON.parse(response.body)
      expect(response).to have_http_status(400)
      expect(json['message']).to eq("Restaurant name can't be blank")
    end
  end
end
