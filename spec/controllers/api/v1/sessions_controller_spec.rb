require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :request do
  let(:user) { FactoryGirl.create :user }

  describe 'POST :create' do

    it 'return user hash for valid user params' do
      post '/api/v1/sessions', params: {auth_token: user.auth_token}
      json = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Signed In')
      expect(json['user']).to match({"id" => user.id,
                                     "provider" => user.provider,
                                     "name" => user.name, 
                                     "image" => user.image, 
                                     "email" => user.email, 
                                     "auth_token" => user.auth_token })
    end
   
    it 'return error when credentials are invalid' do
      user
      post '/api/v1/sessions', params: {auth_token: 'invalid_token'}
      expect_authentication_error(response)
    end
  end

  describe 'GET :check' do
    it 'return return succes for valid user' do
      get '/api/v1/sessions', headers: { Authorization: "Bearer #{user.auth_token}" }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Signed In')
    end

    it 'return return error for invalid user' do
      get '/api/v1/sessions', headers: { Authorization: "Bearer " }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(401)
      expect(json['message']).to eq('Session Expired')
    end
  end
end
