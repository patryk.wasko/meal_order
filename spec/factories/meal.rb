FactoryGirl.define do
  sequence :meal_n do |n|
    "0#{n}"
  end

  factory :meal do
    name { "#{Faker::Food.dish} #{FactoryGirl.generate :meal_n}" }
    price { Faker::Number.decimal(2) }
    user { FactoryGirl.create :user }
    order { FactoryGirl.create :order }
  end
end

