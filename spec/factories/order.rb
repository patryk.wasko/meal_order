FactoryGirl.define do
  sequence :order_n do |n|
    "0#{n}"
  end

  factory :order do
    restaurant_name { "#{Faker::HitchhikersGuideToTheGalaxy.starship} #{FactoryGirl.generate :order_n}" }
  end
end

