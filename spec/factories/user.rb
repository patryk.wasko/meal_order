FactoryGirl.define do
  sequence :provider_uid do |n|
    "123#{n}"
  end

  factory :user do
    email { Faker::Internet.email }
    name { Faker::Name.name }
    provider_uid { FactoryGirl.generate :provider_uid }
    provider 'facebook'
    auth_token { SecureRandom.hex }
  end
end

