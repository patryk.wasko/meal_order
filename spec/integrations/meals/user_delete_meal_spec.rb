require 'rails_helper'

RSpec.feature 'User delete meal', type: :feature do
  let(:user) { FactoryGirl.create :user }
  let(:order) { FactoryGirl.create :order }
  let(:meal) { FactoryGirl.create :meal, user_id: user.id, order_id: order.id }

  before :each do 
    order
    meal
    sign_in_as_user(user)
    expect(page).to have_content('Orders')
    expect(page).to have_content(order.restaurant_name)
    page.find('.btn', text: 'Go to order').click
    expect(page).to have_content(order.restaurant_name)
    expect(page).to have_content(meal.name)
  end

  scenario 'User delete meal from order' do
    within('.list-group-item') do
      click_button('Delete')
    end

    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content(' Are you sure that you want to delete this meal? ')
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Delete')
      expect{click_button('Delete')}.to change{Meal.count}.by(-1)
    end
    expect(page).to_not have_content(meal.name)
  end

  scenario 'User delete already deleted meal' do
    within('.list-group-item') do
      click_button('Delete')
    end
    meal.destroy
    expect(page).to have_css('.modal')
   
    within('.modal') do
      expect(page).to have_content(' Are you sure that you want to delete this meal? ')
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Delete')
      expect{click_button('Delete')}.to_not change{Meal.count}
    end

    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content("Error")
      expect(page).to have_content("Record not found")
      expect(page).to have_selector(:link_or_button, 'Ok')
    end
  end
end
