require 'rails_helper'

RSpec.feature 'User update meal', type: :feature do
  let(:user) { FactoryGirl.create :user }
  let(:order) { FactoryGirl.create :order }
  let(:meal) { FactoryGirl.create :meal, user_id: user.id, order_id: order.id }
  let(:new_meal_name) { 'new meal name' }
  let(:new_meal_price) { '11.11' }

  before :each do 
    order
    meal
    sign_in_as_user(user)
    expect(page).to have_content('Orders')
    expect(page).to have_content(order.restaurant_name)
    page.find('.btn', text: 'Go to order').click
    within('.list-group') do
      expect(page).to have_content(meal.name)
      expect(page).to have_content(meal.price)
      expect(page).to_not have_content(new_meal_name)
      expect(page).to_not have_content(new_meal_price)
    end
  end

  scenario 'User update meal with valid params' do
    within('.list-group-item') do
      click_button('Edit')
    end

    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content('Update meal')
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Update')
      page.find('#meal_name').set new_meal_name
      page.find('#meal_price').set new_meal_price
      expect{click_button('Update')}.to change{Meal.find(meal.id).name}.and change{Meal.find(meal.id).price}
    end

    expect(page).to_not have_css('.modal')

    within('.list-group') do
      expect(page).to_not have_content(meal.name)
      expect(page).to_not have_content(meal.price)
      expect(page).to have_content(new_meal_name)
      expect(page).to have_content(new_meal_price)
    end
  end

  scenario 'User update meal with invalid params' do
    within('.list-group-item') do
      click_button('Edit')
    end

    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content('Update meal')
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Update')
      page.find('#meal_name').set ' '
      page.find('#meal_price').set ' '
      expect{click_button('Update')}.to_not change{Meal.find(meal.id).name}
    end

    within('.modal') do
      expect(page).to have_content("Error")
      expect(page).to have_content("Name can't be blank, Price can't be blank, Price is not a number")
      expect(page).to have_selector(:link_or_button, 'Ok')
    end
  end
end
