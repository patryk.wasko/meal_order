require 'rails_helper'

RSpec.feature 'User add meal to order', type: :feature do
  let(:user) { FactoryGirl.create :user }
  let(:order) { FactoryGirl.create :order }
  let(:meal) { FactoryGirl.create :meal }
  let(:new_meal_name) { 'new meal name' }
  let(:new_meal_price) { '11.11' }

  before :each do 
    order
    sign_in_as_user(user)
    expect(page).to have_content('Orders')
    expect(page).to have_selector(:link_or_button, 'Add meal to order')
  end

  scenario 'User add meal to order on orders page with valid params' do
    within('.list-group-item') do
      click_button('Add meal to order')
    end

    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content('Add meal to order:')
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Add meal')
      page.find('#meal_name').set new_meal_name
      page.find('#meal_price').set new_meal_price
      expect{click_button('Add meal')}.to change{Meal.count}.by(1)
    end
  end

  scenario 'User add meal to order on order page with valid params' do
    page.find('.btn', text: 'Go to order').click
    expect(page).to have_selector(:link_or_button, 'Add meal to order')
    click_button('Add meal to order')

    within('.modal') do
      expect(page).to have_content('Add meal to order:')
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Add meal')
      page.find('#meal_name').set new_meal_name
      page.find('#meal_price').set new_meal_price
      expect{click_button('Add meal')}.to change{Meal.count}.by(1)
    end

    within('.list-group') do
      expect(page).to have_content(new_meal_name)
      expect(page).to have_content(new_meal_price)
      expect(page).to have_content(user.name)
      expect(page).to have_selector(:link_or_button, 'Delete')
      expect(page).to have_selector(:link_or_button, 'Edit')
    end
  end

  scenario 'User add meal to order on order page with invalid params' do
    page.find('.btn', text: 'Go to order').click
    expect(page).to have_selector(:link_or_button, 'Add meal to order')
    click_button('Add meal to order')

    within('.modal') do
      expect(page).to have_content('Add meal to order:')
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Add meal')
      page.find('#meal_name').set ' '
      page.find('#meal_price').set ' '
      expect{click_button('Add meal')}.to_not change{Meal.count}
    end

    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content("Error")
      expect(page).to have_content("Name can't be blank, Price can't be blank, Price is not a number")
      expect(page).to have_selector(:link_or_button, 'Ok')
    end
  end

  scenario 'User add second meal to order on order page with valid params' do
    meal.update(user_id: user.id, order_id: order.id)
    page.find('.btn', text: 'Go to order').click
    expect(page).to have_selector(:link_or_button, 'Add meal to order')
    click_button('Add meal to order')

    within('.modal') do
      expect(page).to have_content('Add meal to order:')
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Add meal')
      page.find('#meal_name').set new_meal_name
      page.find('#meal_price').set new_meal_price
      expect{click_button('Add meal')}.to_not change{Meal.count}
    end

    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content("Error")
      expect(page).to have_content("Order You can add only one meal to order.")
      expect(page).to have_selector(:link_or_button, 'Ok')
    end
  end

  scenario 'User add meal to finalized order on order page with valid params' do
    page.find('.btn', text: 'Go to order').click
    expect(page).to have_selector(:link_or_button, 'Add meal to order')
    click_button('Add meal to order')
    order.finalized!

    within('.modal') do
      expect(page).to have_content('Add meal to order:')
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Add meal')
      page.find('#meal_name').set new_meal_name
      page.find('#meal_price').set new_meal_price
      expect{click_button('Add meal')}.to_not change{Meal.count}
    end

    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content("Error")
      expect(page).to have_content("Order You can add meal only to not finalized order")
      expect(page).to have_selector(:link_or_button, 'Ok')
    end
  end
end
