require 'rails_helper'

RSpec.feature 'User login', type: :feature do
  let(:user) { FactoryGirl.create :user }

  scenario 'Existing user from facebook login with correct user params' do
    omniauth_hash = mock_omniauth_hash('facebook')
    user.update!(provider: omniauth_hash['provider'], provider_uid: omniauth_hash['uid'])

    visit '/'
    within('.login_container') do
      page.find('.btn.btn-facebook').click
    end

    within('.user_panel') do
      expect(page).to have_content(user.email)
      expect(page).to have_content(user.name)
    end
  end

  scenario 'Existing user from github login with correct user params' do
    omniauth_hash = mock_omniauth_hash('github')
    omniauth_hash['provider'] = 'github'
    user.update(provider: omniauth_hash['provider'], provider_uid: omniauth_hash['uid'])
 
    expect(User.last.provider).to eq omniauth_hash['provider']
    visit '/'
    within('.login_container') do
      page.find('.btn.btn-github').click
    end

    within('.user_panel') do
      expect(page).to have_content(user.email)
      expect(page).to have_content(user.name)
    end
  end


  scenario 'New user from facebook login with correct user params' do
    omniauth_hash = mock_omniauth_hash('facebook')

    visit '/'
    within('.login_container') do
      page.find('.btn.btn-facebook').click
    end

    within('.user_panel') do
      expect(page).to have_content(omniauth_hash['info']['email'])
      expect(page).to have_content(omniauth_hash['info']['name'])
    end
  end

  scenario 'New user from github login with correct user params' do
    omniauth_hash = mock_omniauth_hash('github')

    visit '/'
    within('.login_container') do
      page.find('.btn.btn-github').click
    end

    within('.user_panel') do
      expect(page).to have_content(omniauth_hash['info']['name'])
      expect(page).to have_content(omniauth_hash['provider'])
    end
  end
end
