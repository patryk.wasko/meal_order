require 'rails_helper'

RSpec.feature 'User change order status', type: :feature do
  let(:user) { FactoryGirl.create :user }
  let(:order) { FactoryGirl.create :order }

  before :each do 
    order
    sign_in_as_user(user)
    expect(page).to have_content('Orders')
    expect(page).to have_css('.list-group')
    within('.list-group') do
      expect(page).to have_content(order.restaurant_name)
      expect(page).to have_selector(:link_or_button, 'Add meal to order')
      expect(page).to have_selector(:link_or_button, 'Update status')
    end
  end

  scenario 'User change order status' do

    within('.list-group') do
      expect(page).to have_selector(:link_or_button, 'Add meal to order')
    end

    within('.list-group-item') do
      click_button('Update status')
    end
   
    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content(`Update status:`)
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Started')
      expect(page).to have_selector(:link_or_button, 'Finalized')
      expect(page).to have_selector(:link_or_button, 'Ordered')
      expect(page).to have_selector(:link_or_button, 'Delivered')
      expect(page).to have_css('.btn.active', text: 'Started')
      expect{click_button('Finalized')}.to change{Order.find(order.id).status}.from('started').to('finalized')
    end

    expect(page).to_not have_css('.modal')
    within('.list-group') do
      expect(page).to_not have_selector(:link_or_button, 'Add meal to order')
    end

    within('.list-group-item') do
      click_button('Update status')
    end

    within('.modal') do
      expect(page).to_not have_css('.btn.active', text: 'Started')
      expect(page).to have_css('.btn.active', text: 'Finalized')
      expect{click_button('Ordered')}.to change{Order.find(order.id).status}.from('finalized').to('ordered')
    end

    expect(page).to_not have_css('.modal')
    within('.list-group') do
      expect(page).to_not have_selector(:link_or_button, 'Add meal to order')
    end

    within('.list-group-item') do
      click_button('Update status')
    end

    within('.modal') do
      expect(page).to_not have_css('.btn.active', text: 'Finalized')
      expect(page).to have_css('.btn.active', text: 'Ordered')
      expect{click_button('Delivered')}.to change{Order.find(order.id).status}.from('ordered').to('delivered')
    end

    expect(page).to_not have_css('.modal')
    within('.list-group') do
      expect(page).to_not have_selector(:link_or_button, 'Add meal to order')
    end

    within('.list-group-item') do
      click_button('Update status')
    end

    within('.modal') do
      expect(page).to_not have_css('.btn.active', text: 'Ordered')
      expect(page).to have_css('.btn.active', text: 'Delivered')
      expect{click_button('Started')}.to change{Order.find(order.id).status}.from('delivered').to('started')
    end

    expect(page).to_not have_css('.modal')
    within('.list-group') do
      expect(page).to have_selector(:link_or_button, 'Add meal to order')
    end
  end






  scenario 'User change orderd status for already deleted oder' do
    order.destroy
   
    within('.list-group-item') do
      click_button('Update status')
    end
   
    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content(`Update status:`)
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Started')
      expect(page).to have_selector(:link_or_button, 'Finalized')
      expect(page).to have_selector(:link_or_button, 'Ordered')
      expect(page).to have_selector(:link_or_button, 'Delivered')
      expect(page).to have_css('.btn.active', text: 'Started')
      click_button('Finalized')
    end

    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content("Error")
      expect(page).to have_content("Record not found")
      expect(page).to have_selector(:link_or_button, 'Ok')
    end
  end
end
