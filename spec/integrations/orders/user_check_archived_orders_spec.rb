require 'rails_helper'

RSpec.feature 'User check archived order', type: :feature do
  let(:user) { FactoryGirl.create :user }
  let(:order) { FactoryGirl.create :order }
  let(:order_finalized) { FactoryGirl.create :order, status: 'finalized' }
  let(:order_ordered) { FactoryGirl.create :order, status: 'ordered' }
  let(:order_delivered) { FactoryGirl.create :order, status: 'delivered' }

  before :each do 
    order
    order_finalized
    order_ordered
    order_delivered
    sign_in_as_user(user)
    expect(page).to have_content('Orders')
    expect(page).to have_css('.list-group')
    expect(page).to have_selector(:link_or_button, 'Active')
    expect(page).to have_selector(:link_or_button, 'Archived')
    within('.list-group') do
      expect(page).to have_content(order.restaurant_name)
      expect(page).to have_content(order_finalized.restaurant_name)
      expect(page).to have_content(order_ordered.restaurant_name)
      expect(page).to_not have_content(order_delivered.restaurant_name)
    end
  end

  scenario 'User check archived orders' do
    expect(page).to have_css('.btn.active', text: 'Active')
    expect(page).to_not have_css('.btn.active', text: 'Archived')

    click_button('Archived')

    expect(page).to_not have_css('.btn.active', text: 'Active')
    expect(page).to have_css('.btn.active', text: 'Archived')
   
    within('.list-group') do
      expect(page).to_not have_content(order.restaurant_name)
      expect(page).to_not have_content(order_finalized.restaurant_name)
      expect(page).to_not have_content(order_ordered.restaurant_name)
      expect(page).to have_content(order_delivered.restaurant_name)
    end

    click_button('Active')

    expect(page).to have_css('.btn.active', text: 'Active')
    expect(page).to_not have_css('.btn.active', text: 'Archived')

    within('.list-group') do
      expect(page).to have_content(order.restaurant_name)
      expect(page).to have_content(order_finalized.restaurant_name)
      expect(page).to have_content(order_ordered.restaurant_name)
      expect(page).to_not have_content(order_delivered.restaurant_name)
    end
  end
end
