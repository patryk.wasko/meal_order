require 'rails_helper'

RSpec.feature 'User check order page', type: :feature do
  let(:user) { FactoryGirl.create :user }
  let(:order) { FactoryGirl.create :order }
  let(:meal) { FactoryGirl.create :meal, order_id: order.id }

  before :each do 
    order
    meal
    sign_in_as_user(user)
    expect(page).to have_content('Orders')
    expect(page).to have_css('.list-group')
    within('.list-group') do
      expect(page).to have_content(order.restaurant_name)
      expect(page).to have_selector(:link_or_button, 'Go to order')
    end
  end

  scenario 'User check order page' do
    page.find('.btn', text: 'Go to order').click

    expect(page).to have_selector(:link_or_button, 'Back to orders')
    expect(page).to have_selector(:link_or_button, 'Edit')
    expect(page).to have_selector(:link_or_button, 'Delete')
    expect(page).to have_selector(:link_or_button, 'Update status')
    expect(page).to have_selector(:link_or_button, 'Add meal to order')

    expect(page).to have_content(meal.name)
    expect(page).to have_content(meal.price)
    expect(page).to have_content(meal.user.name)
  end
end
