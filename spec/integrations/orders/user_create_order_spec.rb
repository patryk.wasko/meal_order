require 'rails_helper'

RSpec.feature 'User create order', type: :feature do
  let(:user) { FactoryGirl.create :user }
  let(:new_restaurant_name) { 'New restaurant name' }

  before :each do 
    sign_in_as_user(user)
    expect(page).to have_content('Orders')
    expect(page).to have_selector(:link_or_button, 'Add new order')
    click_button('Add new order')
    expect(page).to have_css('.modal')
  end

  scenario 'User create order with valid params' do
    within('.modal') do
      expect(page).to have_content('Add new order:')
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Add order')
      page.find('input').set new_restaurant_name
      expect{click_button('Add order')}.to change{Order.count}.by(1)
    end

    expect(page).to_not have_css('.modal')

    within('.list-group') do
      expect(page).to have_content(new_restaurant_name)
      expect(page).to have_content("Status: started")
      expect(page).to have_selector(:link_or_button, 'Delete')
      expect(page).to have_selector(:link_or_button, 'Update status')
      expect(page).to have_selector(:link_or_button, 'Add meal to order')
      expect(page).to have_selector(:link_or_button, 'Go to order')
    end
  end

  scenario 'User create order with invalid params' do
    within('.modal') do
      page.find('input').set ''
      expect{click_button('Add order')}.to_not change{Order.count}
    end

    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content("Error")
      expect(page).to have_content("Restaurant name can't be blank")
      expect(page).to have_selector(:link_or_button, 'Ok')
    end
  end
end
