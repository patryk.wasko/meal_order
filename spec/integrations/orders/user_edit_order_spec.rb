require 'rails_helper'

RSpec.feature 'User edit order', type: :feature do
  let(:user) { FactoryGirl.create :user }
  let(:order) { FactoryGirl.create :order }
  let(:order_new_restaurant_name) { 'Order new restaurant name' }

  before :each do 
    order
    sign_in_as_user(user)
    expect(page).to have_content('Orders')
    expect(page).to have_css('.list-group')
    within('.list-group') do
      expect(page).to have_content(order.restaurant_name)
      expect(page).to have_selector(:link_or_button, 'Edit')
    end
  end

  scenario 'User edit order with valid params' do

    within('.list-group-item') do
      click_button('Edit')
    end
   
    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content('Update order')
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Update')
      page.find('input').set order_new_restaurant_name
      expect{click_button('Update')}.to change{Order.find(order.id).restaurant_name}.from(order.restaurant_name).to(order_new_restaurant_name)
    end

    expect(page).to_not have_css('.modal')

    within('.list-group-item') do
      expect(page).to_not have_content(order.restaurant_name)
      expect(page).to have_content(order_new_restaurant_name)
      click_button('Update status')
    end
  end

  scenario 'User edit order with invalid params' do
    within('.list-group-item') do
      click_button('Edit')
    end
   
    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content('Update order')
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Update')
      page.find('input').set ' '
      expect{click_button('Update')}.to_not change{Order.find(order.id).restaurant_name}
    end

    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content("Error")
      expect(page).to have_content("Restaurant name can't be blank")
      expect(page).to have_selector(:link_or_button, 'Ok')
    end
  end

  scenario 'User edit order for already deleted oder' do
    order.destroy
   
    within('.list-group-item') do
      click_button('Edit')
    end
   
    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content('Update order')
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Update')
      page.find('input').set order_new_restaurant_name
      click_button('Update')
    end

    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content("Error")
      expect(page).to have_content("Record not found")
      expect(page).to have_selector(:link_or_button, 'Ok')
    end
  end
end
