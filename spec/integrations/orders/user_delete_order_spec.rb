require 'rails_helper'

RSpec.feature 'User delete order', type: :feature do
  let(:user) { FactoryGirl.create :user }
  let(:order) { FactoryGirl.create :order }

  before :each do 
    order
    sign_in_as_user(user)
    expect(page).to have_content('Orders')
    expect(page).to have_css('.list-group')
    within('.list-group') do
      expect(page).to have_content(order.restaurant_name)
      expect(page).to have_selector(:link_or_button, 'Delete')
    end
  end

  scenario 'User delete order' do

    within('.list-group-item') do
      click_button('Delete')
    end
   
    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content(`Are you sure that you want to destroy this order?`)
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Delete')
      expect{click_button('Delete')}.to change{Order.count}.by(-1)
    end

    expect(page).to_not have_css('.modal')
    expect(page).to_not have_content(order.restaurant_name)
  end

  scenario 'User delete already deleted oder' do
    order.destroy
   
    within('.list-group-item') do
      click_button('Delete')
    end

    within('.modal') do
      expect(page).to have_content(`Are you sure that you want to destroy this order?`)
      expect(page).to have_selector(:link_or_button, 'Back')
      expect(page).to have_selector(:link_or_button, 'Delete')
      expect{click_button('Delete')}.to_not change{Order.count}
    end

    expect(page).to have_css('.modal')

    within('.modal') do
      expect(page).to have_content("Error")
      expect(page).to have_content("Record not found")
      expect(page).to have_selector(:link_or_button, 'Ok')
    end
  end
end
