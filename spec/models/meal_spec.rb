require 'rails_helper'

RSpec.describe Meal, type: :model do
  let(:user) { FactoryGirl.create :user }
  let(:order) { FactoryGirl.create :order }
  let(:meal_params) { FactoryGirl.attributes_for :meal, order: order}

  it { is_expected.to have_db_column(:name) }
  it { is_expected.to have_db_column(:price)}

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:price) }
  it { is_expected.to validate_presence_of(:user) }
  it { is_expected.to validate_presence_of(:order) }

  it { is_expected.to belong_to(:order) }
  it { is_expected.to belong_to(:user) }

  describe 'meal order status validation' do 
    before :each do 
      user
    end

    it 'return error when adding meal to finalized order' do
      order.finalized!
      meal = Meal.create(meal_params)
      expect(meal).to be_invalid
      expect(meal.errors[:order]).to include("You can add meal only to not finalized order")
    end

    it 'return error when adding meal to ordered order' do
      order.ordered!
      meal = Meal.create(meal_params)
      expect(meal).to be_invalid
      expect(meal.errors[:order]).to include("You can add meal only to not finalized order")
    end

    it 'return error when adding meal to delivered order' do
      order.delivered!
      meal = Meal.create(meal_params)
      expect(meal).to be_invalid
      expect(meal.errors[:order]).to include("You can add meal only to not finalized order")
    end

    it 'be valid when order started' do
      order.started!
      meal = Meal.create(meal_params)
      expect(meal).to be_valid
    end
  end
end
