require 'rails_helper'

RSpec.describe Order, type: :model do
  it { is_expected.to have_db_column(:restaurant_name) }
  it { is_expected.to have_db_column(:status)}

  it { is_expected.to validate_presence_of(:restaurant_name) }
  it { is_expected.to validate_presence_of(:status) }

  it { is_expected.to have_many(:meals) }
end
