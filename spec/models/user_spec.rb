require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to have_db_column(:email) }
  it { is_expected.to have_db_column(:name) }
  it { is_expected.to have_db_column(:first_name) }
  it { is_expected.to have_db_column(:last_name) }
  it { is_expected.to have_db_column(:image) }
  it { is_expected.to have_db_column(:auth_token) }
  it { is_expected.to have_db_column(:provider_uid) }
  it { is_expected.to have_db_column(:provider) }

  it { is_expected.to validate_uniqueness_of(:auth_token) }
  it { is_expected.to validate_uniqueness_of(:provider_uid).scoped_to(:provider) }

  it { is_expected.to have_db_index(:auth_token) }
  it { is_expected.to have_db_index([:provider_uid, :provider]) }

  it { is_expected.to have_many(:meals) }
end

