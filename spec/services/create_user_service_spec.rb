require 'rails_helper'

RSpec.describe CreateUserService do
  context "Create user service" do
    let(:user) { FactoryGirl.create :user }

    it "Create and return user from omniauth hash" do 
     mock_omniauth_hash
     create_user_service = CreateUserService.new(mock_omniauth_hash)
     expect{user = create_user_service.create_from_omniauth}.to change{User.count}.from(0).to(1)
     expect(user).to eq(User.last)
    end

    it "Return user for existing omniauth hash" do 
     mock_omniauth_hash
     user.update!(provider_uid: mock_omniauth_hash['uid'], 
                  provider: mock_omniauth_hash['provider'])
     create_user_service = CreateUserService.new(mock_omniauth_hash)
     expect{@returned_user = create_user_service.create_from_omniauth}.to_not change{User.count}
     expect(@returned_user).to eq(user)
    end
  end
end
