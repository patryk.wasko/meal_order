Rails.application.routes.draw do
  root to: 'home#index'
   
  get 'sign_in', to: 'home#index'
  get '/orders', to: 'home#index'
  get '/orders/:id', to: 'home#index'
  
  get '/auth/:provider/callback', to: 'session_creator#create_session'

  get 'session_creator', to: 'home#index', as: 'session_creator'

  namespace :api do
    namespace :v1 do
      post '/sessions', to: 'sessions#create'
      get '/sessions', to: 'sessions#check'

      resources :orders, only: [:create, :show, :destroy, :update]
      get 'orders', to: 'orders#index'
      get 'orders_archived', to: 'orders#archived'
      patch 'orders_change_status/:id', to: 'orders#change_status'

      resources :meals, only: [:create, :destroy, :update]
    end
  end
end
