50.times do
  User.create(name: Faker::Name.name,
              email: Faker::Internet.email,
              auth_token: SecureRandom.hex,
              image: Faker::LoremPixel.image,
              provider: 'facebook',
              provider_uid: Faker::Number.number(5)
  )
end

20.times do
  Order.create(restaurant_name: Faker::HitchhikersGuideToTheGalaxy.starship)
end

60.times do
  Meal.create(name: Faker::Food.dish,
              price: Faker::Number.decimal(2), 
              user_id: User.all.sample.id,
              order_id: Order.all.sample.id
 
  )
end
