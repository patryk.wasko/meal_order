class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.string :restaurant_name, null: false
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
