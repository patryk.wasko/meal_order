class AddProviderColumnToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :provider_uid, :string
    add_column :users, :provider, :string
    add_index :users, [:provider_uid, :provider], unique: true
  end
end
